"use strict";

var lenientProperties = ["origin", "searchParams", "toJSON"];
var strictProperties = ["hash", "host", "hostname", "href", "password", "pathname", "port", "protocol", "search", // "toString" excluded because Object::toString exists
"username"];
module.exports = {
  lenientProperties: lenientProperties,
  strictProperties: strictProperties
};
//# sourceMappingURL=props.js.map